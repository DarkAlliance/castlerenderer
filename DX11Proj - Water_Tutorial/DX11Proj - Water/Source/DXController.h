
//
// DXController.h
//

// DXController represents the controller tier of the MVC architecture pattern and references the main window (view), DirectX interfaces and scene variables (model)

#pragma once

#include <GUObject.h>
#include <Windows.h>
#include <buffers.h>
#include <Triangle.h>
#include <Box.h>
#include <Ocean.h>
#include <Grid.h>
class DXSystem;
class GUClock;
class DXModel;
class LookAtCamera;


// CBuffer struct
// Use 16byte aligned so can use optimised XMMathFunctions instead of setting _XM_NO_INNTRINSICS_ define when compiling for x86
__declspec(align(16)) struct CBufferExt  {  
	DirectX::XMMATRIX						WVPMatrix;
	DirectX::XMMATRIX						worldITMatrix; // Correctly transform normals to world space
	DirectX::XMMATRIX						worldMatrix;
	DirectX::XMFLOAT4						eyePos;
	// Simple single light source properties
	DirectX::XMFLOAT4						lightVec; // w=1: Vec represents position, w=0: Vec  represents direction.
	DirectX::XMFLOAT4						lightAmbient;
	DirectX::XMFLOAT4						lightDiffuse;
	DirectX::XMFLOAT4						lightSpecular; 
	FLOAT									Timer;
};


__declspec(align(16)) struct projMatrixStruct  {
	DirectX::XMMATRIX						projMatrix;
};

class DXController : public GUObject {

	HINSTANCE								hInst = NULL;
	HWND									wndHandle = NULL;

	// Strong reference to associated Direct3D device and rendering context.
	DXSystem								*dx = nullptr;

	// Default pipeline stage states
	ID3D11RasterizerState					*defaultRSstate = nullptr;
	ID3D11RasterizerState					*skyRSState = nullptr;
	ID3D11DepthStencilState					*defaultDSstate = nullptr;
	ID3D11BlendState						*defaultBlendState = nullptr;
	ID3D11VertexShader						*skyBoxVS = nullptr;
	ID3D11PixelShader						*skyBoxPS = nullptr;
	ID3D11VertexShader						*reflectionMapVS = nullptr;
	ID3D11PixelShader						*reflectionMapPS = nullptr;
	ID3D11VertexShader						*oceanVS = nullptr;
	ID3D11PixelShader						*oceanPS = nullptr;
	ID3D11Buffer							*cBufferShark = nullptr;
	ID3D11Buffer							*cBufferSky = nullptr;
	ID3D11Buffer							*cBufferWater = nullptr;
	CBufferExt								*cBufferExtSrc = nullptr;

	ID3D11VertexShader						*castleVS = nullptr;
	ID3D11PixelShader						*castlePS = nullptr;
	ID3D11Buffer							*cBufferCastle = nullptr;


	ID3D11VertexShader						*gridVS = nullptr;
	ID3D11PixelShader						*gridPS = nullptr;
	ID3D11Buffer							*cBufferGrid = nullptr;

	// Main FPS clock
	GUClock									*mainClock = nullptr;


	LookAtCamera							*mainCamera = nullptr;
	projMatrixStruct 						*projMatrix = nullptr;

	// Direct3D scene objects
	Box										*skyBox = nullptr;
	Ocean									*water = nullptr;
	DXModel									*shark = nullptr;
	DXModel									*castle = nullptr;
	Grid									*grid = nullptr;

	ID3D11SamplerState						*linearSampler = nullptr;
	// Instances of models that appear in the scene
	ID3D11Texture2D							*castleTexture = nullptr;
	ID3D11ShaderResourceView				*castleTextureSRV = nullptr;
	ID3D11Texture2D							*gridTexture = nullptr;
	ID3D11ShaderResourceView				*gridTextureSRV = nullptr;
	ID3D11Texture2D							*heightMap = nullptr;
	ID3D11ShaderResourceView				*heigthMapSRV = nullptr;

	ID3D11Texture2D							*sharkTexture = nullptr;
	ID3D11ShaderResourceView				*sharkTextureSRV = nullptr;
	ID3D11Texture2D							*cubeMapTexture = nullptr;
	ID3D11ShaderResourceView				*cubeMapTextureSRV = nullptr;
	ID3D11Texture2D							*waterNormalMap = nullptr;
	ID3D11ShaderResourceView				*waterNormalMapSRV = nullptr;

	//
	// Private interface
	//

	// Private constructor
	DXController(const LONG _width, const LONG _height, const wchar_t* wndClassName, const wchar_t* wndTitle, int nCmdShow, HINSTANCE hInstance, WNDPROC WndProc);

	// Return TRUE if the window is in a minimised state, FALSE otherwise
	BOOL isMinimised();


public:

	//
	// Public interface
	//

	// Factory method to create the main DXController instance (singleton)
	static DXController* CreateDXController(const LONG _width, const LONG _height, const wchar_t* wndClassName, const wchar_t* wndTitle, int nCmdShow, HINSTANCE hInstance, WNDPROC WndProc);

	// Destructor
	~DXController();

	// Decouple the encapsulated HWND and call DestoryWindow on the HWND
	void destoryWindow();

	// Resize swap chain buffers and update pipeline viewport configurations in response to a window resize event
	HRESULT resizeResources();

	// Helper function to call updateScene followed by renderScene
	HRESULT updateAndRenderScene();
	HRESULT mapCbuffer(void *cBufferExtSrcL, ID3D11Buffer *cBufferExtL);
	// Clock handling methods
	void startClock();
	void stopClock();
	void reportTimingData();


	//
	// Event handling methods
	//

	// Process mouse move with the left button held down
	void handleMouseLDrag(const POINT &disp);

	// Process mouse wheel movement
	void handleMouseWheel(const short zDelta);

	// Process key down event.  keyCode indicates the key pressed while extKeyFlags indicates the extended key status at the time of the key down event (see http://msdn.microsoft.com/en-gb/library/windows/desktop/ms646280%28v=vs.85%29.aspx).
	void handleKeyDown(const WPARAM keyCode, const LPARAM extKeyFlags);
	
	// Process key up event.  keyCode indicates the key released while extKeyFlags indicates the extended key status at the time of the key up event (see http://msdn.microsoft.com/en-us/library/windows/desktop/ms646281%28v=vs.85%29.aspx).
	void handleKeyUp(const WPARAM keyCode, const LPARAM extKeyFlags);

	

	//
	// Methods to handle initialisation, update and rendering of the scene
	//
	HRESULT rebuildViewport();
	HRESULT initDefaultPipeline();
	HRESULT bindDefaultPipeline();
	HRESULT LoadShader(ID3D11Device *device, const char *filename, DXBlob **PSBytecode, ID3D11PixelShader **pixelShader);
	HRESULT LoadShader(ID3D11Device *device, const char *filename, DXBlob **VSBytecode, ID3D11VertexShader **vertexShader);
	HRESULT initialiseSceneResources();
	HRESULT updateScene();
	HRESULT renderScene();
	HRESULT updateCastle();
	HRESULT updateGrid();
};
