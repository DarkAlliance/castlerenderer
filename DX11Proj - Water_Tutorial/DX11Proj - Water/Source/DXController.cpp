
//
// DXController.cpp
//

#include <stdafx.h>
#include <d3d11shader.h>
#include <d3dcompiler.h>
#include <DXController.h>
#include <DirectXMath.h>
#include <DXSystem.h>
#include <DirectXTK\DDSTextureLoader.h>
#include <DirectXTK\WICTextureLoader.h>
#include <GUClock.h>
#include <DXModel.h>
#include <LookAtCamera.h>


using namespace std;
using namespace DirectX;
using namespace DirectX::PackedVector;
// Load the Compiled Shader Object (CSO) file 'filename' and return the bytecode in the blob object **bytecode.  This is used to create shader interfaces that require class linkage interfaces.
// Taken from DXShaderFactory by Paul Angel. This function has been included here for clarity.
void DXLoadCSO(const char *filename, DXBlob **bytecode)
{

	ifstream	*fp = nullptr;
	DXBlob		*memBlock = nullptr;

	try
	{
		// Validate parameters
		if (!filename || !bytecode)
			throw exception("loadCSO: Invalid parameters");

		// Open file
		fp = new ifstream(filename, ios::in | ios::binary);

		if (!fp->is_open())
			throw exception("loadCSO: Cannot open file");

		// Get file size
		fp->seekg(0, ios::end);
		uint32_t size = (uint32_t)fp->tellg();

		// Create blob object to store bytecode (exceptions propagate up if any occur)
		memBlock = new DXBlob(size);

		// Read binary data into blob object
		fp->seekg(0, ios::beg);
		fp->read((char*)(memBlock->getBufferPointer()), memBlock->getBufferSize());


		// Close file and release local resources
		fp->close();
		delete fp;

		// Return DXBlob - ownership implicity passed to caller
		*bytecode = memBlock;
	}
	catch (exception& e)
	{
		cout << e.what() << endl;

		// Cleanup local resources
		if (fp) {

			if (fp->is_open())
				fp->close();

			delete fp;
		}

		if (memBlock)
			delete memBlock;

		// Re-throw exception
		throw;
	}
}

//
// Private interface implementation
//

// Private constructor
DXController::DXController(const LONG _width, const LONG _height, const wchar_t* wndClassName, const wchar_t* wndTitle, int nCmdShow, HINSTANCE hInstance, WNDPROC WndProc) {

	try
	{
		// 1. Register window class for main DirectX window
		WNDCLASSEX wcex;

		wcex.cbSize = sizeof(WNDCLASSEX);

		wcex.style = CS_DBLCLKS | CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		wcex.hCursor = LoadCursor(NULL, IDC_CROSS);
		wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		wcex.lpszMenuName = NULL;
		wcex.lpszClassName = wndClassName;
		wcex.hIconSm = NULL;

		if (!RegisterClassEx(&wcex))
			throw exception("Cannot register window class for DXController HWND");

		
		// 2. Store instance handle in our global variable
		hInst = hInstance;


		// 3. Setup window rect and resize according to set styles
		RECT		windowRect;

		windowRect.left = 0;
		windowRect.right = _width;
		windowRect.top = 0;
		windowRect.bottom = _height;

		DWORD dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		DWORD dwStyle = WS_OVERLAPPEDWINDOW;

		AdjustWindowRectEx(&windowRect, dwStyle, FALSE, dwExStyle);

		// 4. Create and validate the main window handle
		wndHandle = CreateWindowEx(dwExStyle, wndClassName, wndTitle, dwStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 500, 500, windowRect.right - windowRect.left, windowRect.bottom - windowRect.top, NULL, NULL, hInst, this);

		if (!wndHandle)
			throw exception("Cannot create main window handle");

		ShowWindow(wndHandle, nCmdShow);
		UpdateWindow(wndHandle);
		SetFocus(wndHandle);


		// 5. Initialise render pipeline model (simply sets up an internal std::vector of pipeline objects)
	

		// 6. Create DirectX host environment (associated with main application wnd)
		dx = DXSystem::CreateDirectXSystem(wndHandle);

		if (!dx)
			throw exception("Cannot create Direct3D device and context model");

		// 7. Setup application-specific objects
		HRESULT hr = initialiseSceneResources();

		if (!SUCCEEDED(hr))
			throw exception("Cannot initalise scene resources");


		// 8. Create main clock / FPS timer (do this last with deferred start of 3 seconds so min FPS / SPF are not skewed by start-up events firing and taking CPU cycles).
		mainClock = GUClock::CreateClock(string("mainClock"), 3.0f);

		if (!mainClock)
			throw exception("Cannot create main clock / timer");

	}
	catch (exception &e)
	{
		cout << e.what() << endl;

		// Re-throw exception
		throw;
	}
	
}


// Return TRUE if the window is in a minimised state, FALSE otherwise
BOOL DXController::isMinimised() {

	WINDOWPLACEMENT				wp;

	ZeroMemory(&wp, sizeof(WINDOWPLACEMENT));
	wp.length = sizeof(WINDOWPLACEMENT);

	return (GetWindowPlacement(wndHandle, &wp) != 0 && wp.showCmd == SW_SHOWMINIMIZED);
}



//
// Public interface implementation
//

// Factory method to create the main DXController instance (singleton)
DXController* DXController::CreateDXController(const LONG _width, const LONG _height, const wchar_t* wndClassName, const wchar_t* wndTitle, int nCmdShow, HINSTANCE hInstance, WNDPROC WndProc) {

	static bool _controller_created = false;

	DXController *dxController = nullptr;

	if (!_controller_created) {

		dxController = new DXController(_width, _height, wndClassName, wndTitle, nCmdShow, hInstance, WndProc);

		if (dxController)
			_controller_created = true;
	}

	return dxController;
}


// Destructor
DXController::~DXController() {

	
	//Clean Up- release local interfaces
	// Release RSstate
	defaultRSstate->Release();
	// Release RSstate
	skyRSState->Release();
	// Release dsState
	defaultDSstate->Release();
	// Release blendState
	defaultBlendState->Release();
	// Release VertexShader interface
	skyBoxVS->Release();
	// Release PixelShader interface
	skyBoxPS->Release();


	// Release VertexShader interface
	reflectionMapVS->Release();
	// Release PixelShader interface
	reflectionMapPS->Release();
	// Release VertexShader interface
	oceanVS->Release();
	// Release PixelShader interface
	oceanPS->Release();
	// Release cBuffer
	cBufferShark->Release();
	// Release cBuffer
	cBufferSky->Release();

	// Release castle cbuffer
	cBufferCastle->Release();

	if (cBufferExtSrc)
		_aligned_free(cBufferExtSrc);

	if (projMatrix)
		_aligned_free(projMatrix);


	if (mainCamera)
		mainCamera->release();

	if (mainClock)
		mainClock->release();
	// Release skyBox
	if (shark)
		shark->release();
	// Release skyBox
	if (skyBox)
		skyBox->release();
	
	// Release Box
	if (water)
		water->release();
	// Release castle
	if (castle)
		castle->release();

	if (dx) {

		dx->release();
		dx = nullptr;
	}

	if (wndHandle)
		DestroyWindow(wndHandle);
}


// Decouple the encapsulated HWND and call DestoryWindow on the HWND
void DXController::destoryWindow() {

	if (wndHandle != NULL) {

		HWND hWnd = wndHandle;

		wndHandle = NULL;
		DestroyWindow(hWnd);
	}
}


// Resize swap chain buffers and update pipeline viewport configurations in response to a window resize event
HRESULT DXController::resizeResources() {

	if (dx) {

		// Only process resize if the DXSystem *dx exists (on initial resize window creation this will not be the case so this branch is ignored)
		HRESULT hr = dx->resizeSwapChainBuffers(wndHandle);
		rebuildViewport();
		RECT clientRect;
		GetClientRect(wndHandle, &clientRect);


		if (!isMinimised())
			renderScene();
	}

	return S_OK;
}


// Helper function to call updateScene followed by renderScene
HRESULT DXController::updateAndRenderScene() {

	HRESULT hr = updateScene();

	if (SUCCEEDED(hr))
		hr = renderScene();

	return hr;
}


// Clock handling methods

void DXController::startClock() {

	mainClock->start();
}

void DXController::stopClock() {

	mainClock->stop();
}

void DXController::reportTimingData() {

	cout << "Actual time elapsed = " << mainClock->actualTimeElapsed() << endl;
	cout << "Game time elapsed = " << mainClock->gameTimeElapsed() << endl << endl;
	mainClock->reportTimingData();
}



//
// Event handling methods
//
// Process mouse move with the left button held down
void DXController::handleMouseLDrag(const POINT &disp) {

	mainCamera->rotateElevation((float)-disp.y * 0.01f);
	mainCamera->rotateOnYAxis((float)-disp.x * 0.01f);
}

// Process mouse wheel movement
void DXController::handleMouseWheel(const short zDelta) {

	if (zDelta<0)
		mainCamera->zoomCamera(1.2f);
	else if (zDelta>0)
		mainCamera->zoomCamera(0.9f);
}


// Process key down event.  keyCode indicates the key pressed while extKeyFlags indicates the extended key status at the time of the key down event (see http://msdn.microsoft.com/en-gb/library/windows/desktop/ms646280%28v=vs.85%29.aspx).
void DXController::handleKeyDown(const WPARAM keyCode, const LPARAM extKeyFlags) {

	// Add key down handler here...
}


// Process key up event.  keyCode indicates the key released while extKeyFlags indicates the extended key status at the time of the key up event (see http://msdn.microsoft.com/en-us/library/windows/desktop/ms646281%28v=vs.85%29.aspx).
void DXController::handleKeyUp(const WPARAM keyCode, const LPARAM extKeyFlags) {

	// Add key up handler here...
}



//
// Methods to handle initialisation, update and rendering of the scene
//


HRESULT DXController::rebuildViewport(){
	// Binds the render target view and depth/stencil view to the pipeline.
	// Sets up viewport for the main window (wndHandle) 
	// Called at initialisation or in response to window resize


	ID3D11DeviceContext *context = dx->getDeviceContext();

	if ( !context)
		return E_FAIL;

	// Bind the render target view and depth/stencil view to the pipeline.
	ID3D11RenderTargetView* renderTargetView = dx->getBackBufferRTV();
	context->OMSetRenderTargets(1, &renderTargetView, dx->getDepthStencil());
	// Setup viewport for the main window (wndHandle)
	RECT clientRect;
	GetClientRect(wndHandle, &clientRect);
	D3D11_VIEWPORT viewport;
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.Width = static_cast<FLOAT>(clientRect.right - clientRect.left);
	viewport.Height = static_cast<FLOAT>(clientRect.bottom - clientRect.top);
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	//Set Viewport
	context->RSSetViewports(1, &viewport);
	
	// Compute the projection matrix.
	projMatrix->projMatrix = XMMatrixPerspectiveFovLH(0.25f*3.14, viewport.Width / viewport.Height, 1.0f, 1000.0f);
	return S_OK;
}

HRESULT  DXController::bindDefaultPipeline(){

	ID3D11DeviceContext *context = dx->getDeviceContext();
	if (!context)
		return E_FAIL;
	// Apply RSState
	context->RSSetState(defaultRSstate);
	// Apply dsState
	context->OMSetDepthStencilState(defaultDSstate, 0);
	//Apply blendState
	FLOAT			blendFactor[4]; blendFactor[0] = blendFactor[1] = blendFactor[2] = blendFactor[3] = 1.0f;
	UINT			sampleMask = 0xFFFFFFFF; // Bitwise flags to determine which samples to process in an MSAA context
	context->OMSetBlendState(defaultBlendState, blendFactor, sampleMask);
	return S_OK;
}
HRESULT DXController::initDefaultPipeline(){
	
	ID3D11Device *device = dx->getDevice();
	if (!device)
		return E_FAIL;
	// Initialise default Rasteriser state object
	D3D11_RASTERIZER_DESC			RSdesc;

	ZeroMemory(&RSdesc, sizeof(D3D11_RASTERIZER_DESC));
	// Setup default rasteriser state 
	RSdesc.FillMode = D3D11_FILL_SOLID;
	RSdesc.CullMode = D3D11_CULL_NONE;
	RSdesc.FrontCounterClockwise = TRUE;
	RSdesc.DepthBias = 0;
	RSdesc.SlopeScaledDepthBias = 0.0f;
	RSdesc.DepthBiasClamp = 0.0f;
	RSdesc.DepthClipEnable = TRUE;
	RSdesc.ScissorEnable = FALSE;
	RSdesc.MultisampleEnable = FALSE;
	RSdesc.AntialiasedLineEnable = FALSE;
	HRESULT hr = device->CreateRasterizerState(&RSdesc, &defaultRSstate);
	if (!SUCCEEDED(hr))
		throw std::exception("Cannot create Rasterise state interface");
	
	
	// Sky Box RSState
	RSdesc.CullMode = D3D11_CULL_NONE;
	hr = device->CreateRasterizerState(&RSdesc, &skyRSState);
	if (!SUCCEEDED(hr))
		throw std::exception("Cannot create Rasterise state interface");
	

	// Output - Merger Stage

	// Initialise default depth-stencil state object
	D3D11_DEPTH_STENCIL_DESC	dsDesc;

	ZeroMemory(&dsDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
	// Setup default depth-stencil descriptor
	dsDesc.DepthEnable = TRUE;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	dsDesc.StencilEnable = FALSE;
	dsDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
	dsDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	// Initialise depth-stencil state object based on the given descriptor
	hr = device->CreateDepthStencilState(&dsDesc, &defaultDSstate);
	if (!SUCCEEDED(hr))
		throw std::exception("Cannot create DepthStencil state interface");



	// Initialise default blend state object (Alpha Blending On)
	D3D11_BLEND_DESC	blendDesc;

	ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
	// Setup default blend state descriptor 
	// Add Code Here (Set Alpha Blending On)
	
	blendDesc.AlphaToCoverageEnable = FALSE; // Use pixel coverage info from rasteriser (default)
	blendDesc.IndependentBlendEnable = FALSE; // The following array of render target blend properties uses the blend properties from RenderTarget[0] for ALL render targets

	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;

	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	// Create blendState
	hr = device->CreateBlendState(&blendDesc, &defaultBlendState);
	if (!SUCCEEDED(hr))
		throw std::exception("Cannot create Blend state interface");


	return hr;
}

HRESULT DXController::LoadShader(ID3D11Device *device, const char *filename, DXBlob **PSBytecode, ID3D11PixelShader **pixelShader){

	//Load the compiled shader byte code.
	DXLoadCSO(filename, PSBytecode);
	// Create shader objects
	HRESULT hr = device->CreatePixelShader((*PSBytecode)->getBufferPointer(), (*PSBytecode)->getBufferSize(), NULL, pixelShader);
	
	if (!SUCCEEDED(hr))
		throw std::exception("Cannot create PixelShader interface");
	return hr;
}


HRESULT DXController::LoadShader(ID3D11Device *device, const char *filename, DXBlob **VSBytecode, ID3D11VertexShader **vertexShader){


	//Load the compiled shader byte code.
	DXLoadCSO(filename, VSBytecode);
	HRESULT hr = device->CreateVertexShader((*VSBytecode)->getBufferPointer(), (*VSBytecode)->getBufferSize(), NULL, vertexShader);
	if (!SUCCEEDED(hr))
		throw std::exception("Cannot create VertexShader interface");
	return hr;
}



// Main resource setup for the application.  These are setup around a given Direct3D device.
HRESULT DXController::initialiseSceneResources() {
	//ID3D11DeviceContext *context = dx->getDeviceContext();
	ID3D11Device *device = dx->getDevice();
	if (!device)
		return E_FAIL;

	//
	// Setup main pipeline objects
	//

	// Setup objects for fixed function pipeline stages
	// Rasterizer Stage
	// Bind the render target view and depth/stencil view to the pipeline
	// and sets up viewport for the main window (wndHandle) 


	// Allocate the projection matrix (it is setup in rebuildViewport).
	projMatrix = (projMatrixStruct*)_aligned_malloc(sizeof(projMatrixStruct), 16);

	rebuildViewport();
	initDefaultPipeline();
	bindDefaultPipeline();

	// Setup objects for the programmable (shader) stages of the pipeline
	DXBlob *skyBoxVSBytecode = nullptr;
	DXBlob *skyBoxPSBytecode = nullptr;
	DXBlob *reflectionMapVSBytecode = nullptr;
	DXBlob *reflectionMapPSBytecode = nullptr;
	DXBlob *oceanVSBytecode = nullptr;
	DXBlob *oceanPSBytecode = nullptr;

	DXBlob *castleVSBytecode = nullptr;
	DXBlob *castlePSBytecode = nullptr;

	DXBlob *gridVSBytecode = nullptr;
	DXBlob *gridPSBytecode = nullptr;

	LoadShader(device, "Shaders\\cso\\terrain_vs.cso", &gridVSBytecode, &gridVS);
	LoadShader(device, "Shaders\\cso\\terrain_ps.cso", &gridPSBytecode, &gridPS);


	LoadShader(device, "Shaders\\cso\\reflection_map_vs.cso", &castleVSBytecode, &castleVS);
	LoadShader(device, "Shaders\\cso\\reflection_map_ps.cso", &castlePSBytecode, &castlePS);


	LoadShader(device, "Shaders\\cso\\sky_box_vs.cso", &skyBoxVSBytecode, &skyBoxVS);
	LoadShader(device, "Shaders\\cso\\sky_box_ps.cso", &skyBoxPSBytecode, &skyBoxPS);
	LoadShader(device, "Shaders\\cso\\reflection_map_vs.cso", &reflectionMapVSBytecode, &reflectionMapVS);
	LoadShader(device, "Shaders\\cso\\reflection_map_ps.cso", &reflectionMapPSBytecode, &reflectionMapPS);
	LoadShader(device, "Shaders\\cso\\ocean_vs.cso", &oceanVSBytecode, &oceanVS);
	LoadShader(device, "Shaders\\cso\\ocean_ps.cso", &oceanPSBytecode, &oceanPS);
	
	// Create main camera
	//
	mainCamera = new LookAtCamera();
	mainCamera->setPos(XMVectorSet(25, 2, -35.5, 1));


	// Setup shark CBuffer
	cBufferExtSrc = (CBufferExt*)_aligned_malloc(sizeof(CBufferExt), 16);
	// Initialise shark CBuffer
	cBufferExtSrc->worldMatrix = XMMatrixIdentity();
	cBufferExtSrc->worldITMatrix = XMMatrixIdentity();
	cBufferExtSrc->WVPMatrix = mainCamera->dxViewTransform()*projMatrix->projMatrix;
	cBufferExtSrc->lightVec = XMFLOAT4(-250.0, 130.0, 145.0, 1.0); // Positional light
	cBufferExtSrc->lightAmbient = XMFLOAT4(0.3, 0.3, 0.3, 1.0);
	cBufferExtSrc->lightDiffuse = XMFLOAT4(0.8, 0.8, 0.8, 1.0);
	cBufferExtSrc->lightSpecular = XMFLOAT4(1.0, 1.0, 1.0, 1.0);
	XMStoreFloat4(&cBufferExtSrc->eyePos, mainCamera->getCameraPos());// camera->pos;

	D3D11_BUFFER_DESC cbufferDesc;
	D3D11_SUBRESOURCE_DATA cbufferInitData;
	ZeroMemory(&cbufferDesc, sizeof(D3D11_BUFFER_DESC));
	ZeroMemory(&cbufferInitData, sizeof(D3D11_SUBRESOURCE_DATA));
	cbufferDesc.ByteWidth = sizeof(CBufferExt);
	cbufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	cbufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cbufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbufferInitData.pSysMem = cBufferExtSrc;
	
	//Create shark CBuffer
	HRESULT hr = device->CreateBuffer(&cbufferDesc, &cbufferInitData, &cBufferShark);

	//Create water CBuffer
	hr = device->CreateBuffer(&cbufferDesc, &cbufferInitData, &cBufferWater);

	// Initialise skyBox CBuffer
	hr = device->CreateBuffer(&cbufferDesc, &cbufferInitData, &cBufferSky);
	// Initialize Castle Cbuffer
	hr = device->CreateBuffer(&cbufferDesc, &cbufferInitData, &cBufferCastle);


	// Initialize Grid Cbuffer
	hr = device->CreateBuffer(&cbufferDesc, &cbufferInitData, &cBufferGrid);

	//
	// Setup example objects
	//

	// Load texture
	ID3D11Resource *castleResource = static_cast<ID3D11Resource*> (castleTexture);
	hr = CreateWICTextureFromFile(device, L"Resources\\Textures\\castle_bricks.jpg", &castleResource, &castleTextureSRV);

	ID3D11Resource *gridResource = static_cast<ID3D11Resource*> (gridTexture);
	hr = CreateWICTextureFromFile(device, L"Resources\\Textures\\ground.jpg", &gridResource, &gridTextureSRV);
	ID3D11Resource *hmResource = static_cast<ID3D11Resource*> (heightMap);
	hr = CreateWICTextureFromFile(device, L"Resources\\Textures\\heightmap.jpg", &hmResource, &heigthMapSRV);

	ID3D11Resource *demoModelImageResource = static_cast<ID3D11Resource*>(sharkTexture);
	hr = CreateWICTextureFromFile(device, L"Resources\\Textures\\greatwhiteshark.png", &demoModelImageResource, &sharkTextureSRV);
	ID3D11Resource *cubeMapResource = static_cast<ID3D11Resource*>(cubeMapTexture);
	hr = CreateDDSTextureFromFile(device, L"Resources\\Textures\\Maskonaive2_1024.dds", &cubeMapResource, &cubeMapTextureSRV);
	ID3D11Resource *waterResource = static_cast<ID3D11Resource*>(waterNormalMap);
	hr = CreateDDSTextureFromFile(device, L"Resources\\Textures\\Waves.dds", &waterResource, &waterNormalMapSRV);
	
	dx->getDeviceContext()->PSSetShaderResources(1, 1, &cubeMapTextureSRV);

	shark = new DXModel(device, reflectionMapVSBytecode, wstring(L"Resources\\Models\\shark.obj"), sharkTextureSRV, XMCOLOR(1, 1, 1, 1), XMCOLOR(1, 1, 1, 0.5));
	skyBox = new Box(device, skyBoxVSBytecode, cubeMapTextureSRV);
	water = new Ocean(device, oceanVSBytecode, waterNormalMapSRV);
	castle = new DXModel(device, castleVSBytecode, wstring(L"Resources\\Models\\castle.3ds"), castleTextureSRV, XMCOLOR(1, 1, 1, 1), XMCOLOR(0, 0, 0, 0));
	grid = new Grid(device, gridVSBytecode, gridTextureSRV,heigthMapSRV);

	gridVSBytecode->release();
	gridPSBytecode->release();

	castleVSBytecode->release();
	castlePSBytecode->release();

	// Release PixelShader DXBlob
	skyBoxPSBytecode->release();
	// Release vertexShader DXBlob
	skyBoxVSBytecode->release();
	// Release vertexShader DXBlob
	reflectionMapVSBytecode->release();
	// Release PixelShader DXBlob
	reflectionMapPSBytecode->release();
	// Release vertexShader DXBlob
	oceanVSBytecode->release();
	// Release PixelShader DXBlob
	oceanPSBytecode->release();
	return S_OK;
}

HRESULT DXController::updateCastle()
{
	cBufferExtSrc->worldMatrix = XMMatrixScaling(0.2, 0.2, 0.2)*XMMatrixTranslation(0, 2.8, 0);

	// Update shark cBuffer
	cBufferExtSrc->worldITMatrix = XMMatrixTranspose(XMMatrixInverse(nullptr, cBufferExtSrc->worldMatrix));
	cBufferExtSrc->WVPMatrix = cBufferExtSrc->worldMatrix*mainCamera->dxViewTransform() * projMatrix->projMatrix;
	mapCbuffer(cBufferExtSrc, cBufferCastle);
	return S_OK;
}
HRESULT DXController::updateGrid()
{
	cBufferExtSrc->worldMatrix = XMMatrixScaling(10.0, 10, 10)*XMMatrixTranslation(0, -2.5, 0);

	// Update shark cBuffer
	cBufferExtSrc->worldITMatrix = XMMatrixTranspose(XMMatrixInverse(nullptr, cBufferExtSrc->worldMatrix));
	cBufferExtSrc->WVPMatrix = cBufferExtSrc->worldMatrix*mainCamera->dxViewTransform() * projMatrix->projMatrix;
	mapCbuffer(cBufferExtSrc, cBufferGrid);
	return S_OK;
}
// Update scene state (perform animations etc)
HRESULT DXController::updateScene() {

	ID3D11DeviceContext *context = dx->getDeviceContext();

	mainClock->tick();
	gu_seconds tDelta = mainClock->gameTimeElapsed();

	cBufferExtSrc->Timer = (FLOAT)tDelta;
	XMStoreFloat4(&cBufferExtSrc->eyePos, mainCamera->getCameraPos());

	updateCastle();
	updateGrid();
	// Modify sharks world matrix based on tDelta...
	// Modify Code Here (Make shark "swim" in a circle ) ...
	// After scaling and translating multiply world matrix by a rotation around Y of -tDelta
	// Translate and Rotate shark
	cBufferExtSrc->worldMatrix = XMMatrixScaling(0.5, 0.5, 0.5)*XMMatrixTranslation(25, -6.5, 0)*XMMatrixRotationY(-tDelta); 

	// Update shark cBuffer
	cBufferExtSrc->worldITMatrix = XMMatrixTranspose(XMMatrixInverse(nullptr, cBufferExtSrc->worldMatrix));
	cBufferExtSrc->WVPMatrix = cBufferExtSrc->worldMatrix*mainCamera->dxViewTransform() * projMatrix->projMatrix;
	mapCbuffer(cBufferExtSrc, cBufferShark);
	
	// Update water cBuffer
	// Modify Code Here(Translate the water downwards by 2 units)
	// Scale and translate water world matrix
	cBufferExtSrc->worldMatrix = XMMatrixScaling(8, 8, 8)*XMMatrixTranslation(0, -1.5f, 0);
	cBufferExtSrc->worldITMatrix = XMMatrixTranspose(XMMatrixInverse(nullptr, cBufferExtSrc->worldMatrix));
	cBufferExtSrc->WVPMatrix = cBufferExtSrc->worldMatrix*mainCamera->dxViewTransform() * projMatrix->projMatrix;
	mapCbuffer(cBufferExtSrc, cBufferWater);

	// Update  skyBox cBuffer
	cBufferExtSrc->WVPMatrix = XMMatrixScaling(500, 500, 500)*mainCamera->dxViewTransform()*projMatrix->projMatrix;
	mapCbuffer(cBufferExtSrc, cBufferSky);
	return S_OK;
}

// Helper function to copy cbuffer data from cpu to gpu
HRESULT DXController::mapCbuffer(void *cBufferExtSrcL, ID3D11Buffer *cBufferExtL)
{
	ID3D11DeviceContext *context = dx->getDeviceContext();
	// Map cBuffer
	D3D11_MAPPED_SUBRESOURCE res;
	HRESULT hr = context->Map(cBufferExtL, 0, D3D11_MAP_WRITE_DISCARD, 0, &res);

	if (SUCCEEDED(hr)) {
		memcpy(res.pData, cBufferExtSrcL, sizeof(CBufferExt));
		context->Unmap(cBufferExtL, 0);
	}
	return hr;
}

// Render scene
HRESULT DXController::renderScene() {

	ID3D11DeviceContext *context = dx->getDeviceContext();

	// Validate window and D3D context
	if (isMinimised() || !context)
		return E_FAIL;


	// Clear the screen
	static const FLOAT clearColor[4] = {0.0f, 0.0f, 0.3f, 1.0f };
	context->ClearRenderTargetView(dx->getBackBufferRTV(), clearColor);
	context->ClearDepthStencilView(dx->getDepthStencil(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	
	// set the  skyBox shader objects
	context->VSSetShader(skyBoxVS, 0, 0);
	context->PSSetShader(skyBoxPS, 0, 0);
	// Apply the skyBox cBuffer.
	context->VSSetConstantBuffers(0, 1, &cBufferSky);
	context->PSSetConstantBuffers(0, 1, &cBufferSky);

	// Apply  skyBox RSState
	context->RSSetState(skyRSState);

	// Draw triangle
	if( skyBox) {
		// Render
		skyBox->render(context);
	}

	// Return to default RSState
	context->RSSetState(defaultRSstate);

	context->VSSetShader(gridVS, 0, 0);
	context->PSSetShader(gridPS, 0, 0);

	context->VSSetConstantBuffers(0, 1, &cBufferGrid);
	context->PSSetConstantBuffers(0, 1, &cBufferGrid);
	if (grid)
	{
		grid->render(context);
	}

	context->VSSetShader(castleVS, 0, 0);
	context->PSSetShader(castlePS, 0, 0);

	context->VSSetConstantBuffers(0, 1, &cBufferCastle);
	context->PSSetConstantBuffers(0, 1, &cBufferCastle);
	if (castle)
	{
		castle->render(context);
	}
	// set shader reflection map shaders for shark
	context->VSSetShader(reflectionMapVS, 0, 0);
	context->PSSetShader(reflectionMapPS, 0, 0);

	// Apply the shark cBuffer.
	context->VSSetConstantBuffers(0, 1, &cBufferShark);
	context->PSSetConstantBuffers(0, 1, &cBufferShark);
	// Draw shark
	if (shark) {
		// Render
		shark->render(context);
	}

	// Set ocean vertex and pixel shaders
	context->VSSetShader(oceanVS, 0, 0);
	context->PSSetShader(oceanPS, 0, 0);

	// Apply the cBuffer.
	context->VSSetConstantBuffers(0, 1, &cBufferWater);
	context->PSSetConstantBuffers(0, 1, &cBufferWater);
	// Draw the Water
	if (water) {
		// Render
		water->render(context);
	}

	// Present current frame to the screen
	HRESULT hr = dx->presentBackBuffer();

	return S_OK;
}
